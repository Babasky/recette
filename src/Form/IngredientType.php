<?php

namespace App\Form;

use App\Entity\Ingredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class IngredientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',TextType::class,[
                'attr'=>[
                    'class'=>'form-control',
                    'minlength'=> 10,
                    'maxlength'=> 100
                ],
                'label'=>'Nom de la recette',
                'label_attr' => [
                    'class'=>'form-label'
                ],
                'constraints'=>[
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'min'=> 10,
                        'max'=> 100,
                        //'minmessage'=>'La longueur minimale du nom de la recette doit contenir au moins 10 caractères.',
                        //'maxmessage'=>'La longueur minimale du nom de la recette ne doit pas depasser 100 caractères.',
                    ])
                ]
            ])
            ->add('price', MoneyType::class,[
                'attr'=>[
                    'class'=>'form-control'
                ],
                'label'=>'Prix',
                'label_attr' => [
                    'class'=>'form-label'
                ],
                'constraints'=>[
                    new Assert\Positive(),
                    new Assert\LessThan(500)
                ]
    
            ])
            ->add('submit',SubmitType::class,[
                'label'=>'Ajouter',
                'attr'=>[
                    'class'=>'btn btn-primary mt-4'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ingredient::class,
        ]);
    }
}
