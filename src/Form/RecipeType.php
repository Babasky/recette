<?php

namespace App\Form;

use App\Entity\Ingredient;
use App\Entity\Recipe;
use App\Repository\IngredientRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RecipeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',TextType::class,[
                'attr'=>[
                    'class'=>'form-control',
                    'minlength'=>5,
                    'maxlength'=>100
                ],
                'label'=>'Nom de la recette',
                'label_attr'=>[
                    'class'=>'form-label'
                ],
                'constraints'=>[
                    new Assert\NotNull(),
                    new Assert\Length([
                        'min'=>5,
                        'max'=>100
                    ])
                ]
            ])
            ->add('time',IntegerType::class,[
                'attr'=>[
                    'class'=>'form-control',
                    'min'=>1,
                    'max'=>3600
                ],
                'label'=>'Temps(en minutes)',
                'constraints'=>[
                    new Assert\Positive(),
                    new Assert\LessThan(3600)
                ],
                'required' => false
            ])
            ->add('nbPeople',IntegerType::class,[
                'attr'=>[
                    'class'=>'form-control'
                ],
                'label'=>'Nombre de personnes',
                'required'=>false,
                'constraints'=>[
                    new Assert\Positive(),
                    new Assert\LessThan(51)
                ]
            ])
            ->add('difficulty',RangeType::class,[
                'attr'=>[
                    'class'=>'form-range',
                ],
                'label'=>'Difficulté',
                'required'=>false,
                'constraints'=>[
                    new Assert\Positive(),
                ]
            ])
            ->add('description',TextareaType::class,[
                'attr'=>[
                    'class'=>'form-control'
                ],
                'label'=>'Description',
                'constraints'=>[
                    new Assert\NotBlank()
                ]
            ])
            ->add('price',MoneyType::class,[
                'attr'=>[
                    'class'=>'form-control'
                ],
                'label'=>'Prix',
                'constraints'=>[
                    new Assert\Positive(),
                    new Assert\LessThan(1000)
                ]
            ])
            ->add('isFavorite',RadioType::class,[
                'attr'=>[
                    
                ],
                'label'=>'Favoris ? ',
            ])
            ->add('Ingredients',EntityType::class,[
                'attr'=>[
                    'class'=>'gap-2'
                ],
                'class' => Ingredient::class,
                'query_builder' => function (IngredientRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.name', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('submit',SubmitType::class,[
                'attr'=>[
                    'class'=>'btn btn-primary mt-3'
                ],
                'label'=>'Enregistrer',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Recipe::class,
        ]);
    }
}
