<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use Faker\Generator;
use App\Entity\Recipe;
use App\Entity\Ingredient;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private Generator $faker;
    private UserPasswordHasherInterface $hasher;
    
    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->faker = Factory::create('fr_FR');
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        //Ingrédients
        $ingredients = [];
        for($i=0; $i<=50;$i++){
            $ingredient = new Ingredient();
            $ingredient->setName($this->faker->word(10))
                ->setPrice(mt_rand(200,400));
            
            $ingredients[] = $ingredient;
            
            $manager->persist($ingredient);
        }

        // Recette
        
        for($r=0; $r<=50;$r++){
            $recipe = new Recipe();
            $recipe->setName($this->faker->word(5))
                ->setTime(mt_rand(0,1) === 1 ? mt_rand(100,3600):null)
                ->setNbPeople(mt_rand(0,1) === 1 ? mt_rand(1,5):null)
                ->setDifficulty(mt_rand(0,1)=== 1?mt_rand(1,5):null)
                ->setDescription($this->faker->text(300))
                ->setPrice(mt_rand(200,400))
                ->setIsFavorite(mt_rand(0,1) === 1 ? true:false);
                for($k=0; $k<= mt_rand(5,15);$k++){
                    $recipe->addIngredient($ingredients[mt_rand(0, count($ingredients)-1)]);

                }

                $recipes[] = $recipe;
                $manager->persist($recipe);
            }

            // User

            for ($u=1; $u <= 10 ; $u++) { 
                $user = new User();
                $user->setFullName($this->faker->name())
                ->setPseudo(mt_rand(0,1) === 1 ? $this->faker->firstName() : null)
                ->setEmail($this->faker->email())
                ->setRoles(['ROLE_USER']);
                
            $hashPassword = $this->hasher->hashPassword(
                $user,
                'admin'
            );
            $user->setPassword($hashPassword);
                $manager->persist($user);
                
            }

        $manager->flush();
    }
}
