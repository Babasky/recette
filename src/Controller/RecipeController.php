<?php

namespace App\Controller;

use App\Entity\Recipe;
use App\Form\RecipeType;
use App\Repository\RecipeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RecipeController extends AbstractController
{
    /**
     * Cette methode permet d'afficher toutes les recettes
     *
     * @param RecipeRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */   
    #[Route('/recipe', name: 'app_recipe',methods:['GET'])]
    public function index(RecipeRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        $recipes = $paginator->paginate($repository->findAll(),$request->query->getInt('page', 1),5);
        return $this->render('recipe/index.html.twig', [
            'recipes'=> $recipes
        ]);
    }


    /**
     * function qui permet de créer une recette
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/recipe/new', name: 'app_recipe_new', methods: ['GET','POST'])]
    public function new(Request $request, EntityManagerInterface $manager): Response
    {
        $recipe = new Recipe();
        
        $form = $this->createForm(RecipeType::class, $recipe);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $recipe = $form->getData();
            
            $manager->persist($recipe);
            
            $manager->flush();
            
            $this->addFlash('success', 'Recette ajoutée avec succès');

            return $this->redirectToRoute('app_recipe');
        
        }
        
        return $this->render('recipe/new.html.twig',[
            'form'=> $form->createView()
        ]);
    }


    /**
     * Cette methode permet de modifier une recette
     * @param Recipe $recipe
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */

    #[Route('/recipe/edit/{id}', name: 'app_recipe_edit', methods:['GET','POST'])]
    public function edit(Recipe $recipe, EntityManagerInterface $manager, Request $request): Response
    {
        $form = $this->createForm(RecipeType::class, $recipe);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($recipe);
            $manager->flush();
            $this->addFlash('success','Recette modifiée avec succès');

            return $this->redirectToRoute('app_recipe');
        }
        
        return $this->render('recipe/edit.html.twig',[
            'form' => $form->createView(),
        ]);
    }


    /**
     * cette methode permet de supprimer une recette 
     * @param Recipe $recipe
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    #[Route('/recipe/delete/{id}', name: 'app_recipe_delete', methods:['GET'])]
    public function delete(Recipe  $recipe, EntityManagerInterface $manager , Request $request ):Response
    {
        $manager->remove($recipe);
        $manager->flush();
        $this->addFlash('success','Recette supprimée avec succès');

        return $this->redirectToRoute('app_recipe');
    }
}
