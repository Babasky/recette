<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Form\IngredientType;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IngredientController extends AbstractController
{

    /**
     * Fonction pour afficher tous les ingredients
     *
     * @param IngredientRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    #[Route('/ingredient', name: 'app_ingredient', methods: ['GET'])]
    public function index(IngredientRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        
        $ingredients = $paginator->paginate(
            $repository->findAll(),
            $request->query->getInt('page', 1),5
        );
        
        return $this->render('ingredient/index.html.twig', [
            'ingredients'=>$ingredients
        ]);
    }

    #[Route('/new/ingredient', name: 'app_new_ingredient', methods: ['GET','POST'])]
    public function new(Request $request, EntityManagerInterface $manager): Response
    {
        $ingredient = new Ingredient();
        
        $form = $this->createForm(IngredientType::class, $ingredient);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ingredient = $form->getData();
            $manager->persist($ingredient);
            $manager->flush();
            $this->addFlash('success','Ingrédient ajouté avec succès');

            return $this->redirectToRoute('app_ingredient');
        }

        return $this->render('ingredient/new.html.twig',[
            'form' => $form->createView()
        ]);
    }

    
    /**
     * Cette methode permet de modifier un ingrédient
     * @param Request $request
     * @param EntityManagerInterface $manager
     */


    #[Route('/edit/ingredient/{id}', name: 'app_edit_ingredient', methods: ['GET','POST'])]
    public function edit(Request $request, EntityManagerInterface $manager, Ingredient $ingredient): Response
    {

        $form = $this->createForm(IngredientType::class, $ingredient);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($ingredient);
            $manager->flush();
            $this->addFlash('success','Ingrédient modifié avec succès');

            return $this->redirectToRoute('app_ingredient');
        }
        
        return $this->render('ingredient/edit.html.twig',[
            'form'=>$form->createView()

        ]);
    }



    /**
     * Cette methode permet de supprimer un ingrédient
     * @param EntityManagerInterface $manager
     * @param Ingredient $ingredient
     * @return Response
     */

    #[Route('/delete/ingredient/{id}', name: 'app_delete_ingredient', methods:['GET'])]
    
    public function delete(EntityManagerInterface $manager,Ingredient $ingredient):Response
    {   
        $manager->remove($ingredient);
        $manager->flush();
        
        $this->addFlash('success',"L'ingredient a bien été supprimé");

        return $this->redirectToRoute('app_ingredient');
        
    }
}
